import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = ({ navClass }) => {
  // styling
  const classes = {
    linkClass:
      'w-full h-10 text-center hover:underline text-black font-semibold hover:text-gray-200 visited:bg-red-200 m-1 p-1',
    logoClass:
      'h-16 mt-3 p-1 font-extrabold text-2xl text-black text-center w-full',
    activeLink: 'underline font-extrabold',
  }

  // render
  return (
    <nav className={navClass}>
      <p className={classes.logoClass}>Dashboard</p>
      <NavLink
        className={classes.linkClass}
        activeClassName={classes.activeLink}
        exact
        to="/"
      >
        Dashboard
      </NavLink>
      <NavLink
        className={classes.linkClass}
        activeClassName={classes.activeLink}
        to="/create-player"
      >
        Create Player
      </NavLink>
      <NavLink
        className={classes.linkClass}
        activeClassName={classes.activeLink}
        to="/find"
      >
        Find Player
      </NavLink>
    </nav>
  )
}

export default Navbar
